# Java Barebones Blockchain Client

### What you will need
 * The protoc compiler on your workstation (Google's [protocol buffer compiler](https://github.com/google/protobuf/releases))
 * Mastercard Developers credential
  * **If you dont already have a P12 / Consumerkey for the Blockchain API you can obtain one from [Mastercard Developers](https://developers.mastercard.com)** 
 * Blockchain App Id 
  * **If you dont already have this you obtain one from [Mastercard Developers](https://developers.mastercard.com) by creating a Blockchain project**   

### Getting Started
This application demonstrates the basics of getting started with the Mastercard Core Blockchain API. To get started you should take the following steps 
 * Clone this repository
 * Execute the following commands
   * `mvn package`
   * `java -jar target/java-blockchain-barebones-client-0.0.1-SNAPSHOT-jar-with-dependencies.jar -kp <path to p12> -ck '<consumer key>'` 

When the application starts it gets you to confirm your parameters and ask you for your Blockchain App Id (see above), it then displays a simple menu. From this menu you can create basic text entries on the blockchain and then retrieve them. You can also retrieve the block the entry was written.
 

### Useful Info
This project makes use of the Mastercard Blockchain SDK available from Maven Central. We added this via the pom.xml file

```xml
<dependency>
     <groupId>com.mastercard.api</groupId>
     <artifactId>blockchain</artifactId>
     <version>0.0.1</version>
 </dependency>
```
